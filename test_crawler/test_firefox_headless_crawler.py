from crawler.FirefoxHeadlessCrawler import FirefoxHeadlessCrawler


class TestFirefoxHeadlessCrawler:

    def test_get_company_data_by_name_valid(self):
        firefox_crawler = FirefoxHeadlessCrawler()

        company_data = firefox_crawler.get_company_data_by_name('techbiz')
        assert company_data.rows_data == [
            {
                'Empresa': 'TECHBIZ FORENSE DIGITAL LTDA.',
                'NIRE': '35218345517',
                'Município': 'SAO PAULO'
            },
            {
                'Empresa': 'TECHBIZ CONSULTORIA EM INTELIGENCIA EMPRESARIAL EIRELI',
                'NIRE': '35630211956', 'Município':
                'SAO PAULO'
            },
            {
                'Empresa': 'TECHBIZ FORENSE DIGITAL S.A.',
                'NIRE': '35300366301',
                'Município': 'SAO PAULO'
            },
        ]

        company_data = firefox_crawler.get_company_data_by_name('petrobras')
        assert company_data.rows_data == [
            {'Empresa': 'COOPERATIVA DE ECONOMIA E CREDITO DOS EMPREGADOS DA PETROBRAS - PETROCRED.',
             'NIRE': '35400037211', 'Município': 'SAO PAULO'},
            {'Empresa': 'PETROBRAS GAS S/A GASPETRO', 'NIRE': '33300013806', 'Município': 'RIO DE JANEIRO'},
            {'Empresa': 'PETROBRAS COMERCIO INTERNACIONAL S.A. INTERBRAS', 'NIRE': '33300028609',
             'Município': 'RIO DE JANEIRO'},
            {'Empresa': 'PETROBRAS TRANSPORTE S/A TRANSPETRO', 'NIRE': '33300260391', 'Município': 'RIO DE JANEIRO'},
            {'Empresa': 'PETROBRAS QUIMICA S.A. PETROQUISA', 'NIRE': '33393000882', 'Município': 'RIO DE JANEIRO'},
            {'Empresa': 'CONSORCIO ENTRE MONTCALM E SES PARA RNEST - PETROBRAS', 'NIRE': '35500067383',
             'Município': 'SAO PAULO'},
            {'Empresa': 'CENTRO AUTOMOTIVO PETROBRAS LTDA.', 'NIRE': '35216593301', 'Município': 'RIBEIRAO PRETO'},
            {'Empresa': 'CONSORCIO EMAE/PETROBRAS', 'NIRE': '35500034116', 'Município': 'SAO PAULO'},
            {'Empresa': 'PETROBRAS DISTRIBUIDORA S/A.', 'NIRE': '33300013920', 'Município': 'RIO DE JANEIRO'},
            {'Empresa': 'AUTO POSTO PETROBRAS BEBEDOURO LTDA.', 'NIRE': '35207860768', 'Município': ' '},
            {'Empresa': 'SOCIEDADE PETROLIFERA BRASILEIRA PETROBRAS LTDA.', 'NIRE': '35206778995', 'Município': ' '},
            {'Empresa': 'PETROLEO BRASILEIRO S/A PETROBRAS.', 'NIRE': '33300032061', 'Município': 'RIO DE JANEIRO'},
            {'Empresa': 'RF COMERCIO DE COMBUSTIVEIS LTDA', 'NIRE': '35204042045', 'Município': 'AMERICANA'},
            {'Empresa': 'COOPERATIVA DE CONSUMO DOS TRABALHADORES EM PETROLEO COOPETRO', 'NIRE': '35400015535',
             'Município': ' '},
            {'Empresa': 'CARVAO PETROBRASA LTDA', 'NIRE': '35224780700', 'Município': 'SOROCABA'},
            {'Empresa': 'PETROBRASA LTDA.', 'NIRE': '35206385101', 'Município': ' '},
        ]

        company_data = firefox_crawler.get_company_data_by_name('w9')
        assert company_data.rows_data == [
            {'Empresa': 'W9 INDUSTRIA E COMERCIO DE CONFECCOES EIRELI', 'NIRE': '35601171348',
             'Município': 'SAO PAULO'},
            {'Empresa': 'W9 CONSULTORIA E ASSESSORIA EM GESTAO DE BENEFICIOS LTDA', 'NIRE': '35225247070',
             'Município': 'SAO PAULO'},
            {'Empresa': 'W9 INDUSTRIA E COMERCIO DE CONFECCOES LTDA', 'NIRE': '35225112310', 'Município': 'SAO PAULO'},
            {'Empresa': 'W9 TELECOMUNICACOES LTDA - EPP', 'NIRE': '35223035253', 'Município': 'ITAPETININGA'},
            {'Empresa': 'W9 IT SOLUTIONS LTDA - ME', 'NIRE': '35224046933', 'Município': 'SAO PAULO'},
            {'Empresa': 'W9 CONSTRUCOES & COMERCIO LTDA.', 'NIRE': '35211122342', 'Município': 'SAO PAULO'},
            {'Empresa': 'W9 CONSULTORIA DE TELEMARKETING LTDA - ME', 'NIRE': '35218646096', 'Município': 'AMERICANA'},
            {'Empresa': 'DROGARIA W9 LTDA.', 'NIRE': '35219691257', 'Município': 'SAO PAULO'},
            {'Empresa': 'RENOVENSE ADMINISTRACAO E PRESTACAO DE SERVICOS LTDA.', 'NIRE': '35228898713',
             'Município': 'SAO PAULO'},
            {'Empresa': 'W95 SERVICO E COMERCIO EMPRESARIAL EIRELI', 'NIRE': '35630120080', 'Município': 'SAO PAULO'},
            {'Empresa': 'W9E SEG CORRETORA DE SEGUROS LTDA', 'NIRE': '35235009503', 'Município': 'SAO PAULO'},
            {'Empresa': 'W95 SERVICOS E COMERCIO LTDA.', 'NIRE': '35223404437', 'Município': 'SAO PAULO'},
            {'Empresa': 'W9X EMPREENDIMENTOS E PARTICIPACOES LTDA', 'NIRE': '35224062092', 'Município': 'SAO PAULO'},
        ]

        company_data = firefox_crawler.get_company_data_by_name('TECHBIZ CONSULTORIA EM INTELIGENCIA EMPRESARIAL EIRELI TECHBIZ '
                                                                'CONSULTORIA EM INTELIGENCIA EMPRESARI')
        assert company_data.rows_data == [
            {
                'Empresa': 'TECHBIZ CONSULTORIA EM INTELIGENCIA EMPRESARIAL EIRELI',
                'NIRE': '35630211956', 'Município':
                'SAO PAULO'
            },
        ]

        firefox_crawler.quit()

    def test_get_company_data_by_name_invalid(self):
        firefox_crawler = FirefoxHeadlessCrawler()

        company_data = firefox_crawler.get_company_data_by_name('cinnecta')
        assert company_data == "Nenhuma empresa encontrada com nome cinnecta."

        company_data = firefox_crawler.get_company_data_by_name(35218345517)
        assert company_data == "Nome a ser buscado deve ser do tipo string."

        company_data = firefox_crawler.get_company_data_by_name("G")
        assert company_data == "Nome a ser buscado deve conter pelo menos 2 caracteres e no máximo 100."

        company_data = firefox_crawler.get_company_data_by_name(
            "TECHBIZ CONSULTORIA EM INTELIGENCIA EMPRESARIAL EIRELI TECHBIZ "
            "CONSULTORIA EM INTELIGENCIA EMPRESARIi")
        assert company_data == "Nome a ser buscado deve conter pelo menos 2 caracteres e no máximo 100."

        company_data = firefox_crawler.get_company_data_by_name("0211956")
        assert company_data == "Nome a ser buscado deve comter pelo menos uma letra."

        company_data = firefox_crawler.get_company_data_by_name("@!0211956")
        assert company_data == "Nome a ser buscado deve comter pelo menos uma letra."

        firefox_crawler.quit()

    def test_get_company_data_by_nire_valid(self):
        firefox_crawler = FirefoxHeadlessCrawler()

        company_data = firefox_crawler.get_company_data_by_nire('35218345517')
        assert company_data.get_details_dict() == {
            'nome': 'TECHBIZ FORENSE DIGITAL LTDA.',
            'tipo de empresa': 'SOCIEDADE LIMITADA',
            'inícío de atividade': '2003-06-04',
            'cnpj': '05.757.597/0001-37', 'nire': '35218345517',
            'data de constituição': '2003-07-02',
            'objeto': 'Desenvolvimento e licenciamento de programas de computador customizáveis\nConsultoria em tecnologia da '
                      'informação\nTratamento de dados, provedores de serviços de aplicação e serviços de hospedagem '
                      'na internet\nTreinamento em informática',
            'capital': 'R$ 600.000,00 (Seiscentos Mil Reais)',
            'logradouro': 'Rua Olimpiadas',
            'número': '205',
            'bairro': 'Vila Olimpia',
            'complemento': 'Sala 436',
            'município': 'Sao Paulo',
            'cep': '04551-000'
        }

        company_data = firefox_crawler.get_company_data_by_nire('35225210133')
        assert company_data.get_details_dict() == {
            'nome': 'GOOGLE BRASIL PAGAMENTOS LTDA.',
            'tipo de empresa': 'SOCIEDADE LIMITADA',
            'inícío de atividade': '2011-02-04',
            'cnpj': '13.382.906/0001-60',
            'nire': '35225210133',
            'data de constituição': '2011-02-25',
            'objeto': 'Outras atividades de prestação de serviços de informação não especificadas anteriormente\nOutras '
                      'atividades de serviços prestados principalmente às empresas não especificadas anteriormente\nOutras '
                      'sociedades de participação, exceto holdings\nAtividades de cobrança e informações cadastrais',
            'capital': 'R$ 2.000.000,00 (Dois Milhões De Reais)',
            'logradouro': 'Avenida Brigadeiro Faria Lima',
            'número': '3477',
            'bairro': 'Itaim Bibi',
            'complemento': '19 And T Sul',
            'município': 'Sao Paulo',
            'cep': '04538-133'
        }

        firefox_crawler.quit()

    def test_get_company_data_by_nire_invalid(self):
        firefox_crawler = FirefoxHeadlessCrawler()

        company_data = firefox_crawler.get_company_data_by_nire('35218345519')
        assert company_data == "Nenhuma empresa encontrada com NIRE 35218345519."

        company_data = firefox_crawler.get_company_data_by_nire('352183455178')
        assert company_data == "NIRE a ser buscado deve conter 11 caracteres numéricos."

        company_data = firefox_crawler.get_company_data_by_nire('3521834551')
        assert company_data == "NIRE a ser buscado deve conter 11 caracteres numéricos."

        company_data = firefox_crawler.get_company_data_by_nire(35300366301)
        assert company_data == "NIRE a ser buscado deve ser do tipo string."

        firefox_crawler.quit()


