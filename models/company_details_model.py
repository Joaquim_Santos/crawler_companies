import re
from typing import Dict


class CompanyDescriptionsModel:
    def __init__(self, company_name: str, company_type: str,constitution_date: str, activity_date: str, cnpj: str,
                 object: str, capital: str, public_place: str, neighborhood: str, county: str, number: int,
                 complement: str, cep: str, nire: str):
        self.company_name = company_name
        self.company_type = company_type
        self.constitution_date = constitution_date
        self.activity_date = activity_date
        self.cnpj = cnpj
        self.nire = nire
        self.object = object
        self.capital = capital
        self.address = CompanyAddressModel(public_place, neighborhood, county, number, complement, cep)

    @property
    def company_name(self) -> str:
        return self.__company_name

    @company_name.setter
    def company_name(self, company_name):
        self.__company_name = company_name

    @property
    def company_type(self) -> str:
        return self.__company_type

    @company_type.setter
    def company_type(self, company_type):
        self.__company_type = company_type

    @property
    def constitution_date(self) -> str:
        return self.__constitution_date

    @constitution_date.setter
    def constitution_date(self, constitution_date):
        if constitution_date and (re.compile(r'[0-9]{2}/[0-9]{2}/[0-9]{4}').match(constitution_date)):
            constitution_date = constitution_date.split("/")
            self.__constitution_date = f'{constitution_date[2]}-{constitution_date[1]}-{constitution_date[0]}'
        else:
            self.__constitution_date = ''

    @property
    def activity_date(self) -> str:
        return self.__activity_date

    @activity_date.setter
    def activity_date(self, activity_date):
        if activity_date and (re.compile(r'[0-9]{2}/[0-9]{2}/[0-9]{4}').match(activity_date)):
            activity_date = activity_date.split("/")
            self.__activity_date = f'{activity_date[2]}-{activity_date[1]}-{activity_date[0]}'
        else:
            self.__activity_date = ''

    @property
    def cnpj(self) -> str:
        return self.__cnpj

    @cnpj.setter
    def cnpj(self, cnpj):
        if cnpj and (re.compile(r'[0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2}')\
                     .match(cnpj)):
            self.__cnpj = cnpj
        else:
            self.__cnpj = ''

    @property
    def nire(self) -> str:
        return self.__nire

    @nire.setter
    def nire(self, nire):
        self.__nire = nire

    @property
    def object(self) -> str:
        return self.__object

    @object.setter
    def object(self, object):
        self.__object = object

    @property
    def capital(self) -> str:
        return self.__capital

    @capital.setter
    def capital(self, capital):
        self.__capital = capital

    def get_details_dict(self) -> Dict:
        return {
            "nome": self.company_name,
            "tipo de empresa": self.company_type,
            "inícío de atividade": self.activity_date,
            "cnpj": self.cnpj,
            "nire": self.nire,
            "data de constituição": self.constitution_date,
            "objeto": self.object,
            "capital": self.capital,
            **self.address.get_address_dict()
        }

    def __repr__(self):
        details = '{\n'
        for field, value in self.get_details_dict().items():
            details = f'{details}{field}: {value},\n'
        details = details + '}'

        return details


class CompanyAddressModel:
    def __init__(self, public_place: str, neighborhood: str, county: str, number: int, complement: str, cep: str):
        self.public_place = public_place
        self.number = number
        self.neighborhood = neighborhood
        self.county = county
        self.complement = complement
        self.cep = cep

    @property
    def public_place(self) -> str:
        return self.__public_place

    @public_place.setter
    def public_place(self, public_place):
        self.__public_place = public_place

    @property
    def number(self) -> str:
        return self.__number

    @number.setter
    def number(self, number):
        self.__number = number

    @property
    def neighborhood(self) -> str:
        return self.__neighborhood

    @neighborhood.setter
    def neighborhood(self, neighborhood):
        self.__neighborhood = neighborhood

    @property
    def county(self) -> str:
        return self.__county

    @county.setter
    def county(self, county):
        self.__county = county

    @property
    def complement(self) -> str:
        return self.__complement

    @complement.setter
    def complement(self, complement):
        self.__complement = complement

    @property
    def cep(self) -> str:
        return self.__cep

    @cep.setter
    def cep(self, cep):
        if cep and (re.compile(r'[0-9]{5}[-]?[0-9]{3}').match(cep)):
            self.__cep = cep
        else:
            self.__cep = ''

    def get_address_dict(self) -> Dict:
        return {
            "logradouro": self.public_place,
            "número": self.number,
            "bairro": self.neighborhood,
            "complemento": self.complement,
            "município": self.county,
            "cep": self.cep
        }
