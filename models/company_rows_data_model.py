from typing import List, Dict


class CompanyRowDataModel:

    def __init__(self, company: str, nire: str, county: str):
        self.company = company
        self.nire = nire
        self.county = county

    @property
    def company(self) -> str:
        return self.__company

    @company.setter
    def company(self, company):
        self.__company = company

    @property
    def nire(self) -> str:
        return self.__nire

    @nire.setter
    def nire(self, nire):
        self.__nire = nire.replace(" ", "")

    @property
    def county(self) -> str:
        return self.__county

    @county.setter
    def county(self, county):
        self.__county = county

    def get_row_dict(self) -> Dict:
        return {
                "Empresa": self.company,
                "NIRE": self.nire,
                "Município": self.county
                }


class CompanyRowsModel:

    def __init__(self):
        self.__rows_data = []

    @property
    def rows_data(self) -> List:
        return self.__rows_data

    def add_row_data(self, row: CompanyRowDataModel):
        self.__rows_data.append(row.get_row_dict())

    def __repr__(self):
        rows = '[\n'
        for row in self.rows_data:
            rows = f'{rows}{row},\n'
        rows = f'{rows}]'

        return rows