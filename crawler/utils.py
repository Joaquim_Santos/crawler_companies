def write_crawler_result(search_type: str, term: str, search_result: str):
    with open("crawler_results.txt", "a") as result:
        result.write(f"Resultado da busca por {search_type}: {term}\n")
        result.write("============================================\n\n")
        result.write(f'{search_result}\n\n')
        result.write("============================================\n\n")