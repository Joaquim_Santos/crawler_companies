from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import cv2
from models.company_rows_data_model import CompanyRowDataModel, CompanyRowsModel
from models.company_details_model import CompanyDescriptionsModel
from crawler.utils import write_crawler_result
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from unidecode import unidecode
import re
import time


class WebElement:
    def __init__(self, type, value):
        self.type = type
        self.value = value


class FirefoxHeadlessCrawler(Firefox):
    def __init__(self, executable_path="geckodriver"):
        self.__options = Options()
        self.__options.headless = False
        self.captcha_image = WebElement(By.XPATH, '//img[contains(@src, "CaptchaImage")]')
        self.captcha_textbox = WebElement(By.CSS_SELECTOR, '.inputText')
        self.captcha_button = WebElement(By.CSS_SELECTOR, 'input#ctl00_cphContent_gdvResultadoBusca_btEntrar')
        self.search_text_field = WebElement(By.ID, 'ctl00_cphContent_frmBuscaSimples_txtPalavraChave')
        self.result_table_by_name = WebElement(By.ID, 'ctl00_cphContent_gdvResultadoBusca_gdvContent')
        self.page_button = WebElement(By.ID, 'ctl00_cphContent_gdvResultadoBusca_pgrGridView_btrNext_lbtText')
        self.result_table_by_nire_base = WebElement(By.ID, 'ctl00_cphContent_frmPreVisualiza_lblEmpresa')
        self.no_results = WebElement(By.ID, "ctl00_cphContent_gdvResultadoBusca_qtpGridview_lblMessage")
        super().__init__(options=self.__options, executable_path=executable_path)

    def wait_for_element_in_page(self, web_element, timeout=5):
        try:
            WebDriverWait(self, timeout).until(
                EC.presence_of_element_located((web_element.type, web_element.value)))
            return self.find_element(web_element.type, web_element.value)
        except (NoSuchElementException, TimeoutException):
            return False

    def verify_and_fill_captcha(self, pass_condition):
        while True:
            captcha_image = self.wait_for_element_in_page(self.captcha_image)
            if not captcha_image:
                break
            captcha_image.screenshot("captcha.png")

            img = cv2.imread('captcha.png')
            img = cv2.resize(img, (500, 137))

            cv2.imshow('Confira os digitos da imagem, feche-a e informe-os', img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            code_captcha = input("Informe o código da imagem: ")

            captcha_field = self.wait_for_element_in_page(self.captcha_textbox)
            captcha_field.send_keys(code_captcha)

            captcha_button = self.wait_for_element_in_page(self.captcha_button)
            captcha_button.click()

            if self.wait_for_element_in_page(pass_condition):
                break
            elif self.wait_for_element_in_page(self.no_results):
                break

    def search_company_data(self, company_name_or_nire, pass_condition):
        self.get("https://www.jucesponline.sp.gov.br/")

        field_search = self.wait_for_element_in_page(self.search_text_field)
        if not field_search:
            return False
        field_search.send_keys(company_name_or_nire)

        search_button = self.find_element_by_id("ctl00_cphContent_frmBuscaSimples_btPesquisar")
        search_button.click()

        self.verify_and_fill_captcha(pass_condition)
        return True

    def get_company_data_by_name(self, company_name):
        if not isinstance(company_name, str):
            return "Nome a ser buscado deve ser do tipo string."
        elif not "".join(re.findall("[a-z]+", unidecode(company_name.lower()))):
            return "Nome a ser buscado deve comter pelo menos uma letra."
        elif len(company_name) < 2 or len(company_name) > 100:
            return "Nome a ser buscado deve conter pelo menos 2 caracteres e no máximo 100."

        if not self.search_company_data(company_name, self.result_table_by_name):
            return "Não foi possível carregar os elementos corretamente na página."

        if self.wait_for_element_in_page(self.no_results):
            return f"Nenhuma empresa encontrada com nome {company_name}."

        company_rows_model = CompanyRowsModel()

        while True:
            result_table = self.wait_for_element_in_page(self.result_table_by_name)
            if not result_table:
                return "Não foi possível carregar os elementos corretamente na página."
            rows = result_table.find_elements_by_tag_name("tr")

            for row in rows[1:]:
                data = row.find_elements_by_tag_name("td")
                company_rows_model.add_row_data(CompanyRowDataModel(data[1].text, data[0].text, data[2].text))

            next_page = self.wait_for_element_in_page(self.page_button)
            if not next_page:
                break
            next_page.click()
            time.sleep(1)

        write_crawler_result("Nome", company_name, company_rows_model.__repr__())
        return company_rows_model

    def get_field_by_id(self, id):
        try:
            return self.find_element_by_id(id).text
        except NoSuchElementException:
            return ''

    def get_company_data_by_nire(self, company_nire):
        if not isinstance(company_nire, str):
            return "NIRE a ser buscado deve ser do tipo string."
        elif not company_nire.isdigit():
            return "NIRE a ser buscado deve conter apenas números."
        elif len(company_nire) != 11:
            return "NIRE a ser buscado deve conter 11 caracteres numéricos."

        if not self.search_company_data(company_nire, self.result_table_by_nire_base):
            return "Não foi possível carregar os elementos corretamente na página."

        if self.wait_for_element_in_page(self.no_results):
            return f"Nenhuma empresa encontrada com NIRE {company_nire}."

        fields = []
        base_id = self.result_table_by_nire_base.value[:36]

        for id in ["Empresa", "Detalhes", "Constituicao", "Atividade", "Cnpj", "Objeto", "Capital", "Logradouro",
                   "Bairro", "Municipio", "Numero", "Complemento", "Cep"]:
            fields.append(self.get_field_by_id(base_id+id))

        fields.append(company_nire)
        company_description_model = CompanyDescriptionsModel(*fields)

        write_crawler_result("NIRE", company_nire, company_description_model.__repr__())
        return company_description_model

    def start(self):
        while True:
            option = input("informe a opção desejada para o Crawler\
                            \n1 - Buscar pelo Nome da empresa\
                            \n2 - Buscar pelo NIRE da empresa\
                            \nQualquer outra tecla - Sair\
                            \nSua opção: ")

            if option == '1':
                company_name = input("Informe o nome da empresa: ")
                self.get_company_data_by_name(company_name)
            elif option == '2':
                company_nire = input("Informe o NIRE da empresa: ")
                self.get_company_data_by_nire(company_nire)
            else:
                self.quit()
                break


if __name__ == '__main__':
    firefox_crawler = FirefoxHeadlessCrawler("../geckodriver")
    result_by_name = firefox_crawler.get_company_data_by_name('w9')
    print(result_by_name)

    result_by_nire = firefox_crawler.get_company_data_by_nire('35218345517')
    print(result_by_nire)
    firefox_crawler.quit()
