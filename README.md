1. Para executar o preojeto, é necessário ter o geckodriver 
(driver do firexox) no diretório raíz do projeto. 
Obter versão correspondente em: https://github.com/mozilla/geckodriver/releases

2. Esse crawler é interativo com o usuário, sendo assim, os termos de busca
devem ser informados pelo mesmo, havendo a opção de busca po nome ou NIRE,
também solicitada ao usuário.

3. Outra interação é para preencher as captchas. Durante a busca, o crawler pode
parar e solicitar isso. Nesse caso, será exibida uma imagem com 
as letras a serem informadas (se a imagem não aparecer na tela, estará em outra janela).
Deve-se verificar quais são as letras, fechar a janela da imagem e digitá-las no programa.

4. Além do resultado no print, também é gerado um arquivo crawler_results.txt
com todos os resultados de busca efetuados.